import java.util.ArrayList;
import java.util.Random;

public class SpielerHand{
    public String[] spielerNamen;
    public ArrayList<ArrayList<UnoKarte>> spielerHaende;

    public SpielerHand(int lenght, String[] PlayerNames) {
        spielerNamen = new String[lenght];
        spielerHaende = new ArrayList<ArrayList<UnoKarte>>();
        for (int i = 0; i < lenght; i++) {
            spielerNamen[i] = PlayerNames[i];
            spielerHaende.add(new ArrayList<UnoKarte>());
            //Diese Methode erstellt zu jedem Spieler eine ArrayListe
        }
    }

    public void fuegeKarteHinzu(int spielerIndex, UnoDeck deck, int kartenIndex) {
        UnoKarte karte = deck.get(kartenIndex);
        spielerHaende.get(spielerIndex).add(karte);
        deck.hand.remove(kartenIndex);
        //Diese Fuktion fügt karten in die Hand des jeweiligen spielers
    }

    public void entferneKarteHand(int spielerIndex, int kartenIdex, UnoDeck deck, int zug){
        Regelwerk regel = new Regelwerk();
        kartenIdex = kartenIdex-1;
        regel.aktuelleKarte(spielerHaende.get(spielerIndex).get(kartenIdex));
        deck.hand.add(spielerHaende.get(spielerIndex).get(kartenIdex));
        spielerHaende.get(spielerIndex).remove(kartenIdex);
        //Diese Methode entfernt eine Karte aus der Hand des Spielers
    }

    public void zeigeHaende(int indexSpieler) {
            int j = 1;
            for (UnoKarte karte : spielerHaende.get(indexSpieler)) {
                System.out.print("Index: " + j +"\t");
                j++;
            }
        //Diese Methode zeigt die Hand des Spielers
    }
    public void befuelleSpieleHand(int spielerIndex, UnoDeck deck, int anzahl){
        Random rand = new Random();
        for (int i = 0; i < anzahl; i++) {
            int n = rand.nextInt(deck.hand.size());
            fuegeKarteHinzu(spielerIndex, deck, n);
        }
        //Diese Methode gibt die Anzhal der Karten die der Spieler bekommen soll und wählt die zufähligge karte aus
    }
    public void ranking(){
        for (int i = 0; i < spielerHaende.size(); i++){
        }
    }
}


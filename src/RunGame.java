/*
    An dieser Datei hat Luan Blum größtenteils gearbeitet
 */

import java.util.ArrayList;

public class RunGame {

    public boolean Interface = true;

    UnoDeck deck = new UnoDeck();
    Regelwerk regel = new Regelwerk();
    farbAuswahl farbAuswahl = new farbAuswahl();
    EndScreen Ende = new EndScreen();
    final Object lock = new Object();
    final Object locked = new Object();
    private int lenght = 0;
    public ArrayList<Boolean> isBot;
    public BotInterface anzeige;


    public void Start() {

        PlayerNamesInput Names = new PlayerNamesInput(locked);

        synchronized (locked) {
            try {
                locked.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        //Es wird so Lange gewartet bis die Eingabe der Spieler Namen abgeschlossen ist


        lenght = Names.getgroesse();

        isBot = new ArrayList<Boolean>();
        isBot = Names.getisBot();

        if (lenght > 4) {
            deck.befuellen();
            deck.befuellen();
        } else {
            deck.befuellen();
        }
        //Bei mehr als 4 Spilern sind 108 Karten zu wenig, deswegen werden die Karten verdoppelt (116 Karten)!
        deck.mischeHand();
        regel.wunschFestlegen();


        String[] PlayerNames = new String[lenght];
        int i;
        for (i = 0; i < lenght; i++) {
            PlayerNames[i] = Names.getNames(i);
        }
        SpielerHand spielerhand = new SpielerHand(lenght, PlayerNames);
        for (i = 0; i < lenght; i++) {
            spielerhand.befuelleSpieleHand(i, deck, 7);
            spielerhand.zeigeHaende(i);
        }
        regel.reihenfolgeFestlegen(lenght);
        //Die Spieler Namen werden verarbeitet und jeder Spieler bekommt die Hand gefüllt
        GameLoop(spielerhand);
    }

    public void GameLoop(SpielerHand spielerHand) {
        boolean win = false;
        int zug = 1;
        int legeKarte;
        int spieler;
        int reihen = 0;
        UnoKarte aktuelleKarte = regel.ersteKarte(deck, zug, deck.hand.size());
        boolean geht = false;   //Das boolean hilft bei der kontrolle ob die Karte legbar ist!!!!

        UnoSpielfeld spielFeld = new UnoSpielfeld(aktuelleKarte, spielerHand, regel,lock,lenght,isBot);


        if(Interface == true){
            anzeige = new BotInterface();
        }

        BotKontroll botKontroll = new BotKontroll();
        botKontroll.setBotKontroll(spielerHand.spielerHaende.size());

        while (!win) {
            spielFeld.aktuelleKarte(aktuelleKarte);
            reihen = regel.reihenfolge();
            spielFeld.colorPlayer(reihen);
            boolean start = false;
            spielFeld.highZugStarten();
            while (start != true) {
                start = spielFeld.getzugStarten();
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                //Die Schleife Wartet bis der Zug vom Spiler gestartet wird

            }
            spielFeld.highZugStartenundo();
            if(isBot.get(reihen) == false){
                spielFeld.KarteHinzufuegen(spielerHand, reihen);
                spielFeld.aktuelleKarte(aktuelleKarte);
                boolean kartelegen = false;
                boolean karteziehen = false;
                while ((kartelegen || karteziehen) != true) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }

                    kartelegen = spielFeld.getkartelegen();
                    karteziehen = spielFeld.getkarteziehen();
                    //Diese Schleife wartet bis der Spieler sich zwischen Karte Legen und Karte Ziehen entschieden hat
                }
                if (kartelegen == true) {
                    UnoKarte neueKarte = spielerHand.spielerHaende.get(spielFeld.getSpielerIndex()).get(spielFeld.getKartenIndex());
                    geht = true;

                    //Entscheidet der Spieler sich für das legen der Karte wird wird die zu legende Karte entnommen

                    if (geht == regel.karteLegbar(aktuelleKarte, neueKarte, deck, spielerHand, lenght)) {
                        spielerHand.entferneKarteHand(spielFeld.getSpielerIndex(), spielFeld.getKartenIndex() + 1, deck, zug);
                        aktuelleKarte = neueKarte;
                        farbAuswahl.farbAuswahl(aktuelleKarte, regel, spielFeld);
                        geht = false;

                        //Hier wird die Karte dann geprüft ob es möglich ist diese zu legen

                    } else {

                        //Ist es nicht möglich die Karte zu legen, läuft der Loop hier weiter
                        while (geht == true) {
                            kartelegen = false;
                            karteziehen = false;
                            while ((kartelegen || karteziehen) != true) {
                                try {
                                    Thread.sleep(100);
                                } catch (InterruptedException e) {
                                    throw new RuntimeException(e);
                                }
                                kartelegen = spielFeld.getkartelegen();
                                karteziehen = spielFeld.getkarteziehen();

                                //Diese Schleife prüft erneut ob der Spieler eine Karte Legen oder ziehen möchte

                            }
                            if (kartelegen == true) {
                                neueKarte = spielerHand.spielerHaende.get(spielFeld.getSpielerIndex()).get(spielFeld.getKartenIndex());
                                //Entscheidet der Spieler sich für das legen der Karte wird wird die zu legende Karte erneut entnommen

                                if (geht == regel.karteLegbar(aktuelleKarte, neueKarte, deck, spielerHand, lenght)) {
                                    spielerHand.entferneKarteHand(spielFeld.getSpielerIndex(), spielFeld.getKartenIndex() + 1, deck, zug);
                                    aktuelleKarte = neueKarte;
                                    farbAuswahl.farbAuswahl(aktuelleKarte, regel, spielFeld);
                                    geht = false;

                                    //Hier wird erneut geprüft ob die zu legende Karte legbar ist
                                }
                            } else if (karteziehen == true) {
                                spielerHand.befuelleSpieleHand(spielFeld.getSpielerIndex(), deck, 1);
                                spielFeld.changeKarteziehen(spielerHand.spielerHaende.get(spielFeld.getSpielerIndex()).get(spielerHand.spielerHaende.get(spielFeld.getSpielerIndex()).size() - 1));

                                // Entscheidet der Spieler sich für das zihen der Karte wird ihm eine Karte hinzugefügt

                                synchronized (lock) {
                                    try {
                                        lock.wait();
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }

                                while (geht == true) {
                                    kartelegen = false;
                                    karteziehen = false;
                                    spielFeld.highLegen();
                                    spielFeld.highBehalten();
                                    while ((kartelegen || karteziehen) != true) {
                                        try {
                                            Thread.sleep(100);
                                        } catch (InterruptedException e) {
                                            throw new RuntimeException(e);
                                        }
                                        kartelegen = spielFeld.getneuekartelegen();
                                        karteziehen = spielFeld.getneuekartebehalten();

                                        //Hier wird entschieden ob der Spieler die neu gezogene Karte behalten möchte oder direkt legen möchte
                                    }
                                    spielFeld.highLegenundo();
                                    spielFeld.highBehaltenundo();
                                    if (karteziehen == true) {
                                        spielFeld.changeKarteziehenundo();
                                        geht = false;

                                        //möchte der Spieler die Karte behalten passiert quasi nichts mehr

                                    } else if (kartelegen == true) {
                                        neueKarte = spielerHand.spielerHaende.get(spielFeld.getSpielerIndex()).get(spielerHand.spielerHaende.get(spielFeld.getSpielerIndex()).size() - 1);
                                        if (geht == regel.karteLegbar(aktuelleKarte, neueKarte, deck, spielerHand, lenght)) {
                                            aktuelleKarte = neueKarte;
                                            farbAuswahl.farbAuswahl(aktuelleKarte, regel, spielFeld);
                                            spielerHand.entferneKarteHand(spielFeld.getSpielerIndex(), spielerHand.spielerHaende.get(spielFeld.getSpielerIndex()).size(), deck, zug);
                                            spielFeld.changeKarteziehenundo();

                                            //Hier findet die Prüfung ob die Karte legbar ist statt
                                        }
                                        geht = false;
                                    }
                                }
                            }
                        }
                    }
                } else if (karteziehen == true) {
                    geht = true;
                    spielerHand.befuelleSpieleHand(spielFeld.getSpielerIndex(), deck, 1);
                    spielFeld.changeKarteziehen(spielerHand.spielerHaende.get(spielFeld.getSpielerIndex()).get(spielerHand.spielerHaende.get(spielFeld.getSpielerIndex()).size() - 1));
                    synchronized (lock) {
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    while (geht == true) {
                        kartelegen = false;
                        karteziehen = false;
                        spielFeld.highBehalten();
                        spielFeld.highLegen();
                        while ((kartelegen || karteziehen) != true) {
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                                throw new RuntimeException(e);
                            }
                            kartelegen = spielFeld.getneuekartelegen();
                            karteziehen = spielFeld.getneuekartebehalten();
                            //Hier wird erneut entschieden ob der Spieler die Karte behalten oder legen möchte

                        }
                        spielFeld.highBehaltenundo();
                        spielFeld.highLegenundo();
                        if (karteziehen == true) {
                            spielFeld.changeKarteziehenundo();
                            geht = false;

                            //Behält der Spieler die Karte, so passiert quasi nichts

                        } else if (kartelegen == true) {
                            UnoKarte neueKarte = spielerHand.spielerHaende.get(spielFeld.getSpielerIndex()).get(spielerHand.spielerHaende.get(spielFeld.getSpielerIndex()).size() - 1);
                            if (geht == regel.karteLegbar(aktuelleKarte, neueKarte, deck, spielerHand, lenght)) {
                                aktuelleKarte = neueKarte;
                                farbAuswahl.farbAuswahl(aktuelleKarte, regel, spielFeld);
                                spielerHand.entferneKarteHand(spielFeld.getSpielerIndex(), spielerHand.spielerHaende.get(spielFeld.getSpielerIndex()).size(), deck, zug);
                                spielFeld.changeKarteziehenundo();

                                //legt er sie wid sie überprüft oob dies auch möglich ist. Ist es nicht möglich behält der Spiler di Karte und der loop läuft weiter
                            }
                            geht = false;
                        }
                    }
                }
                if (spielerHand.spielerHaende.get(spielFeld.getSpielerIndex()).size() == 1) {

                    //besitzt der Spieler nur noch eine Karte, so muss er uno "Sagen"
                    //Dies passiert indem der spieler inerhalb von 2.5 Sekunden auf uno drücken muss, sonst bekommt er eine Karte als Strafe

                    spielFeld.highUno();
                    boolean eineKarte = false;
                    try {
                        Thread.sleep(2500);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    spielFeld.highUnoundo();
                    eineKarte = spielFeld.getUno();
                    if (eineKarte == true) {
                        spielFeld.aktuelleKarte(aktuelleKarte);
                        spielFeld.Kartenausschalten();
                        continue;
                    } else {
                        spielerHand.befuelleSpieleHand(spielFeld.getSpielerIndex(), deck, 1);
                    }
                }
                //Besitzt ein Spiler keine Karten mehr, so wird der loop beendet und der EndScreen wird aufgerufen

                if (spielerHand.spielerHaende.get(spielFeld.getSpielerIndex()).size() == 0) {
                    spielFeld.beenden();
                    win = true;
                    Ende.EndScreen(spielerHand, spielFeld.getSpielerIndex());
                }
                spielFeld.changeKarteziehenundo();
                spielFeld.aktuelleKarte(aktuelleKarte);
                spielFeld.Kartenausschalten();
            }else if (isBot.get(reihen) == true){
                int kartenIndex = botKontroll.BotKontroll(spielerHand,reihen,aktuelleKarte,regel,deck, spielFeld,anzeige,Interface);
                if(kartenIndex != -1){
                    UnoKarte neueKarte = spielerHand.spielerHaende.get(reihen).get(kartenIndex);
                    if(regel.karteLegbar(aktuelleKarte,neueKarte,deck,spielerHand,lenght) == true){
                        aktuelleKarte = neueKarte;
                        spielerHand.entferneKarteHand(reihen,kartenIndex + 1,deck,zug);
                        spielFeld.aktuelleKarte(aktuelleKarte);
                    }
                }
                if(spielerHand.spielerHaende.get(regel.reihenfolge.get(1)).size() == 0) {
                    spielFeld.beenden();
                    win = true;
                    Ende.EndScreen(spielerHand, regel.reihenfolge.get(1));
                }
                spielFeld.changeKarteziehenundo();
                spielFeld.aktuelleKarte(aktuelleKarte);
                spielFeld.Kartenausschalten();
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
}

/*
    An dieser Datei hat Furkan Celik größtenteils gearbeitet
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class EndScreen extends Thread {
    public void EndScreen(SpielerHand spielerHand, int spielerIndex){
        JFrame frame = new JFrame();
        frame.setTitle("End Screen");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());

        JPanel ende1 = new JPanel(new BorderLayout());
        frame.add(ende1, BorderLayout.CENTER);

        JPanel ende2 = new JPanel(new BorderLayout());
        frame.add(ende2, BorderLayout.NORTH);

        JPanel ende3 = new JPanel(new BorderLayout());
        ende2.add(ende3, BorderLayout.CENTER);

        ende1.setLayout(new BoxLayout(ende1, BoxLayout.X_AXIS));

        JTextArea Sieger = new JTextArea();
        int zahl = spielerIndex;
        Sieger.setText("Der Gewinner ist: \n Player" + (zahl + 1) + " " + spielerHand.spielerNamen[spielerIndex]);
        ende3.add(Sieger);
        Sieger.setFont(new Font("Arial", Font.PLAIN, 40));

        JButton NeueRundeButton = new JButton("Neue Runde");
        ende1.add(NeueRundeButton);
        NeueRundeButton.setFont(new Font("Arial", Font.PLAIN, 30));
        NeueRundeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
                    protected Void doInBackground() throws Exception {
                        UNO.main(new String[0]);
                        return null;
                    }
                };
                worker.execute();
                frame.dispose();
            }
        });

        JButton Beenden = new JButton("Beenden");
        ende1.add(Beenden);
        Beenden.setFont(new Font("Arial", Font.PLAIN, 30));
        Beenden.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        frame.pack();
        frame.setVisible(true);
    }
    //Es wird ein EndScreen erstellt der den Siger anzeigt.
    //Es gibt den Knopf "Neue Runde", was eine neue Runde beginnen lässt
    //Es gibt den Knopf "Beenden", was das Spiel Beendet
}

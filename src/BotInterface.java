

//Daran hat Justin Klein gearbeitet

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
public class BotInterface extends JFrame{
    private JPanel kartenPanel1;
    private JPanel kartenPanel2;
    private JLabel BotLabel;
    public BotInterface(){
        setTitle("Bot Überblick");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        kartenPanel1 = new JPanel();
        JScrollPane kartenScrollPanel1;
        kartenPanel1.setLayout(new FlowLayout(FlowLayout.LEFT));
        kartenScrollPanel1 = new JScrollPane(kartenPanel1);
        add(kartenScrollPanel1, BorderLayout.NORTH);

        kartenPanel2 = new JPanel();
        JScrollPane kartenScrollPanel2;
        kartenPanel2.setLayout(new FlowLayout(FlowLayout.LEFT));
        kartenScrollPanel2 = new JScrollPane(kartenPanel2);
        add(kartenScrollPanel2, BorderLayout.CENTER);

        BotLabel = new JLabel();
        add(BotLabel,BorderLayout.SOUTH);

        setSize(800,700);
        setVisible(true);
        revalidate();
        repaint();
    }
    public void anzeige(ArrayList<UnoKarte> LegbareKarten,SpielerHand spielerHand, int aktuellerSpieler,String Bot){

        aktuelleKarteHinzufuegen(spielerHand,aktuellerSpieler);

        legbareKarteHinzufuegen(LegbareKarten);

        BotLabel.setText(Bot);
        BotLabel.setFont(new Font("Arial", Font.PLAIN, 20));
    }

    public void aktuelleKarteHinzufuegen( SpielerHand spielerHand, int aktuellerSpieler){
        int size = spielerHand.spielerHaende.get(aktuellerSpieler).size()-1;
        kartenPanel1.removeAll();
        revalidate();
        repaint();
        for (int i = 0; i <= size; i++) {
            JButton kartenButton = createImageButton("UnoKarten/"+Bild(spielerHand.spielerHaende.get(aktuellerSpieler).get(i))+".png"); // Passe den Pfad zum Kartenbild an
            kartenPanel1.add(kartenButton);
        }
    }
    public void legbareKarteHinzufuegen(ArrayList<UnoKarte> LegbareKarten){
        int size = LegbareKarten.size()-1;
        kartenPanel2.removeAll();
        revalidate();
        repaint();
        for (int i = 0; i <= size; i++) {
            JButton kartenButton = createImageButton("UnoKarten/"+Bild(LegbareKarten.get(i))+".png"); // Passe den Pfad zum Kartenbild an
            kartenPanel2.add(kartenButton);
        }
    }
    public JButton createImageButton(String imagePath) {
        ImageIcon icon = new ImageIcon(imagePath);
        JButton button = new JButton(icon);
        button.setBorderPainted(false);
        button.setFocusPainted(false);
        button.setContentAreaFilled(false);
        return button;
    }
    public String Bild(UnoKarte aktuelleKarte){
        Farbe farbe = aktuelleKarte.getFarbe();
        Wert wert = aktuelleKarte.getWert();
        String wort = farbe + "_" + wert;
        return wort;

    }
}

/*
    An dieser Datei hat Alexandros Tzialidiss größtenteils gearbeitet
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.text.PlainDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;

public class PlayerNamesInput extends JFrame {
    private JTextField[] nameFields;
    private JCheckBox[] botCheckboxes;

    protected ArrayList<String> Names;
    protected ArrayList<Boolean> isBot;

    Object locked1 = null;


    public PlayerNamesInput(Object locked) {
        locked1 = locked;

        setTitle("Spielernamen eingeben");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new FlowLayout());

        JLabel playerCountLabel = new JLabel("Anzahl der Spieler:");
        add(playerCountLabel);

        SpinnerNumberModel spinnerModel = new SpinnerNumberModel(2, 2, 8, 1); // Spieleranzahl zwischen 2 und 8
        JSpinner playerCountSpinner = new JSpinner(spinnerModel);
        add(playerCountSpinner);

        JButton playButton = new JButton("Play");
        playButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int playerCount = (int) playerCountSpinner.getValue();
                showPlayerNamesInput(playerCount);
            }
        });

        add(playButton);

        setSize(200,100);
        setLocationRelativeTo(null);
        setVisible(true);
        //Es wird ein fenster mit der größe 200 x 200 erstellt, indem man zwischen 2 und 8 Spielern auswählen kann
    }

    private void showPlayerNamesInput(int playerCount) {
        getContentPane().removeAll();
        setLayout(new GridLayout(playerCount + 1, 3));

        JLabel[] labels = new JLabel[playerCount];
        nameFields = new JTextField[playerCount];
        botCheckboxes = new JCheckBox[playerCount];

        for (int i = 0; i < playerCount; i++) {
            labels[i] = new JLabel("Spieler " + (i + 1) + ": ");
            nameFields[i] = new JTextField(10);
            int maxLength = 10; // Maximale Länge des Textfelds
            nameFields[i].setDocument(new PlainDocument() {
                @Override
                public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {
                    if (str == null)
                        return;

                    if ((getLength() + str.length()) <= maxLength) {
                        super.insertString(offset, str, attr);
                    }
                }
            });
            botCheckboxes[i] = new JCheckBox("Bot");

            JPanel panel = new JPanel(new FlowLayout());
            panel.add(labels[i]);
            panel.add(nameFields[i]);
            panel.add(botCheckboxes[i]);

            add(panel);
        }

        JButton submitButton = new JButton("Submit");
        submitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Names = new ArrayList<String>();
                isBot = new ArrayList<Boolean>();

                for (int i = 0; i < playerCount; i++) {
                    Names.add(nameFields[i].getText());
                    isBot.add(botCheckboxes[i].isSelected());
                }

                //showPlayerNames(playerNames, isBot);

                synchronized (locked1) {
                    locked1.notify();
                }
                dispose();

            }
        });

        add(submitButton);

        setSize(800,800);
        revalidate();
        repaint();

        //Es wird ein fenster erstellt, mit der größe 800 x 800.
        //In diesem Fenster befindet sich für jeden Spiler ein eingabe Feld für den jeweiligen Namen
        //Zusätzlich gibt es ein Hacken. Damit lässt sich auswählen ob der "Spieler" eine Bot sein wird
    }


    private void showPlayerNames(String[] playerNames, boolean[] isBot) {
        getContentPane().removeAll();
        setLayout(new FlowLayout());

        JLabel playerLabel = new JLabel("Spieler:");
        add(playerLabel);

        for (int i = 0; i < playerNames.length; i++) {
            JLabel nameLabel = new JLabel(playerNames[i] + (isBot[i] ? " (Bot)" : ""));
            add(nameLabel);
        }

        setSize(800, 700);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public String getNames(int i){
        return Names.get(i);
    }

    public int getgroesse(){
        return Names.size();
    }

    public ArrayList<Boolean> getisBot(){
        return isBot;
    }

}

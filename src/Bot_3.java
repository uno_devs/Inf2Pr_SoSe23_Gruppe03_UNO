
/*
    der Bot versucht durch legen von +2 und +4 Karten den Spieler daran zu hindern das spiel zu gewinnen
 */
// An diesem Bot hat Justin Klein gearbeitet
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Bot_3 {

    boolean isZwei = false;
    boolean isVier = false;
    boolean isMulticolor = false;
    boolean neuKarte = false;
    Farbe zwingendeFarbe;

    private ArrayList<UnoKarte> LegbareKarten;
    public int Bot_3(SpielerHand spielerHand,int aktuellerSpieler, UnoKarte aktuelleKarte, Regelwerk regel,UnoDeck deck, UnoSpielfeld spielfeld,BotInterface anzeige,boolean Interface){
        LegbareKarten = new ArrayList<UnoKarte>();
        legbareKarten(spielerHand,aktuellerSpieler,aktuelleKarte,regel);
        sortiereUnoKarten(LegbareKarten);
        int lenght = spielerHand.spielerHaende.get(aktuellerSpieler).size();
        int size = LegbareKarten.size();
        if(Interface == true) {
            String Bot = "Bot 3";
            anzeige.anzeige(LegbareKarten, spielerHand, aktuellerSpieler,Bot);
        }
        int anzahlspielerKarten = spielerHand.spielerHaende.get(regel.reihenfolge.get(0)).size();

        for(UnoKarte karte : spielerHand.spielerHaende.get(aktuellerSpieler)){
            if(karte.getWert() == Wert.Draw_Four){
                isVier = true;
            } else if (karte.getWert() == Wert.Zwei_Ziehen) {
                isZwei = true;
            }
        }
        int anzahl = 0;
        for(UnoKarte karten : spielerHand.spielerHaende.get(aktuellerSpieler)){
            if (isZwei == true || isVier == true){
                if (karten.getWert() == Wert.Zwei_Ziehen || karten.getWert() == Wert.Draw_Four){
                    anzahl += 1;
                }
            }
        }
        if(anzahl == lenght){
            boolean passt = false;
            if(LegbareKarten.size() >= 1) {
                for (UnoKarte karten : LegbareKarten){
                    if(ueberpruefeKarte(aktuelleKarte,karten,regel) == true){
                        aktuelleKarte = karten;
                        neuKarte = true;
                        passt = true;
                        break;
                    }
                }
            }
            if(passt == false){
                spielerHand.befuelleSpieleHand(aktuellerSpieler,deck,1);
                lenght = spielerHand.spielerHaende.get(aktuellerSpieler).size();
                UnoKarte neueKarte = spielerHand.spielerHaende.get(aktuellerSpieler).get(lenght-1);
                if(ueberpruefeKarte(aktuelleKarte,neueKarte,regel)){
                    aktuelleKarte = neueKarte;
                    neuKarte = true;
                }
            }
        }else if(anzahlspielerKarten > 2){
            if(isZwei == true || isVier == true) {
                boolean passend = false;
                if(LegbareKarten.size() >= 1) {
                    for (UnoKarte karte : LegbareKarten) {
                        if (karte.getWert() != Wert.Draw_Four && karte.getWert() != Wert.Zwei_Ziehen) {
                            if(ueberpruefeKarte(aktuelleKarte,karte,regel)){
                                aktuelleKarte = karte;
                                neuKarte = true;
                                passend = true;
                            }
                        }
                    }
                }
                if(passend == false){
                    spielerHand.befuelleSpieleHand(aktuellerSpieler,deck,1);
                    lenght = spielerHand.spielerHaende.get(aktuellerSpieler).size();
                    if(spielerHand.spielerHaende.get(aktuellerSpieler).get(lenght-1).getWert() != Wert.Draw_Four && spielerHand.spielerHaende.get(aktuellerSpieler).get(lenght-1).getWert() != Wert.Zwei_Ziehen) {
                        if (ueberpruefeKarte(aktuelleKarte, spielerHand.spielerHaende.get(aktuellerSpieler).get(lenght - 1), regel) == true) {
                            aktuelleKarte = spielerHand.spielerHaende.get(aktuellerSpieler).get(lenght - 1);
                            neuKarte = true;
                        }
                    }
                }
            }else{
                spielerHand.befuelleSpieleHand(aktuellerSpieler,deck,1);
                lenght = spielerHand.spielerHaende.get(aktuellerSpieler).size();
                if(spielerHand.spielerHaende.get(aktuellerSpieler).get(lenght-1).getWert() != Wert.Draw_Four && spielerHand.spielerHaende.get(aktuellerSpieler).get(lenght-1).getWert() != Wert.Zwei_Ziehen) {
                    if (ueberpruefeKarte(aktuelleKarte, spielerHand.spielerHaende.get(aktuellerSpieler).get(lenght - 1), regel) == true) {
                        aktuelleKarte = spielerHand.spielerHaende.get(aktuellerSpieler).get(lenght - 1);
                        neuKarte = true;
                    }
                }
            }
        }else if(anzahlspielerKarten <= 2) {
            if(isZwei == true){
                int menge = spielerHand.spielerHaende.get(aktuellerSpieler).size();
                for(int j = 0; j < menge; j++) {
                    UnoKarte karte = spielerHand.spielerHaende.get(aktuellerSpieler).get(j);
                    if(karte.getWert() == Wert.Zwei_Ziehen) {
                        if (ueberpruefeKarte(aktuelleKarte, karte, regel) == true) {
                            aktuelleKarte = karte;
                            neuKarte = true;
                            break;
                        }else {
                            for (int i = 0; i < menge; i++){
                                UnoKarte karte2 = spielerHand.spielerHaende.get(aktuellerSpieler).get(i);
                                if(karte2.getFarbe() == karte.getFarbe() && aktuelleKarte.getWert() == karte2.getWert()){
                                    aktuelleKarte = karte2;
                                    neuKarte = true;
                                    break;
                                } else if (karte2.getFarbe() == Farbe.Wild && karte2.getWert() == Wert.Multicolor) {
                                    aktuelleKarte = karte2;
                                    neuKarte = true;
                                    isMulticolor = true;
                                    zwingendeFarbe = karte.getFarbe();
                                    break;
                                }else{
                                    spielerHand.befuelleSpieleHand(aktuellerSpieler,deck,1);
                                    lenght = spielerHand.spielerHaende.get(aktuellerSpieler).size();
                                    UnoKarte neuekarte = spielerHand.spielerHaende.get(aktuellerSpieler).get(lenght-1);
                                    if(ueberpruefeKarte(aktuelleKarte,neuekarte,regel)){
                                        aktuelleKarte = neuekarte;
                                        neuKarte = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }else if(isVier == true){
                for(UnoKarte karte : spielerHand.spielerHaende.get(aktuellerSpieler)){
                    if(karte.getWert() == Wert.Draw_Four){
                        aktuelleKarte = karte;
                        neuKarte = true;
                        break;
                    }
                }
            }else{
                spielerHand.befuelleSpieleHand(aktuellerSpieler,deck,1);
                lenght = spielerHand.spielerHaende.get(aktuellerSpieler).size();
                UnoKarte neuekarte = spielerHand.spielerHaende.get(aktuellerSpieler).get(lenght-1);
                if(ueberpruefeKarte(aktuelleKarte,neuekarte,regel)){
                    aktuelleKarte = neuekarte;
                    neuKarte = true;
                }
            }
        }
        int kartenIndex = -1;
        if(neuKarte == true) {
            if (regel.color.size() == 2) {
                if (aktuelleKarte.getFarbe() == regel.color.get(1) || aktuelleKarte.getFarbe() == regel.color.get(0)) {
                    if (aktuelleKarte.getFarbe() == regel.color.get(0)) {
                        regel.color.remove(1);
                    }
                    lenght = spielerHand.spielerHaende.get(aktuellerSpieler).size();
                    for (int i = 0; i < lenght; i++) {
                        if (aktuelleKarte.equals(spielerHand.spielerHaende.get(aktuellerSpieler).get(i))) {
                            kartenIndex = i;
                        }
                    }
                }
            } else {
                lenght = spielerHand.spielerHaende.get(aktuellerSpieler).size();
                for (int i = 0; i < lenght; i++) {
                    if (aktuelleKarte.equals(spielerHand.spielerHaende.get(aktuellerSpieler).get(i))) {
                        kartenIndex = i;
                    }
                }
            }
        }
        if(isMulticolor == true){
            regel.color.add(zwingendeFarbe);
            spielfeld.wechsleWunschKarte(zwingendeFarbe,Wert.Multicolor);
        }else if (neuKarte == true) {
            int[] farben = {0, 0, 0, 0, 0};
            if (aktuelleKarte.getWert() == Wert.Draw_Four || aktuelleKarte.getWert() == Wert.Multicolor) {
                for (UnoKarte karte : spielerHand.spielerHaende.get(aktuellerSpieler)) {
                    switch (karte.getFarbe()) {
                        case Gruen -> farben[0] += 1;
                        case Gelb -> farben[1] += 1;
                        case Blau -> farben[2] += 1;
                        case Rot -> farben[3] += 1;
                        case Wild -> farben[4] += 1;
                    }
                }
                int max = farben[0];
                int id = 0;

                for (int i = 1; i < farben.length; i++) {
                    if (farben[i] > max) {
                        max = farben[i];
                        id = i;
                    }
                }
                switch (id) {
                    case 0 -> {
                        regel.color.add(Farbe.Gruen);
                        if (Wert.Draw_Four == aktuelleKarte.getWert()) {
                            spielfeld.wechsleWunschKarte(Farbe.Gruen, Wert.Draw_Four);
                        } else if (Wert.Multicolor == aktuelleKarte.getWert()) {
                            spielfeld.wechsleWunschKarte(Farbe.Gruen, Wert.Multicolor);
                        }
                    }
                    case 1 -> {
                        regel.color.add(Farbe.Gelb);
                        if (Wert.Draw_Four == aktuelleKarte.getWert()) {
                            spielfeld.wechsleWunschKarte(Farbe.Gelb, Wert.Draw_Four);
                        } else if (Wert.Multicolor == aktuelleKarte.getWert()) {
                            spielfeld.wechsleWunschKarte(Farbe.Gelb, Wert.Multicolor);
                        }
                    }
                    case 2 -> {
                        regel.color.add(Farbe.Blau);
                        if (Wert.Draw_Four == aktuelleKarte.getWert()) {
                            spielfeld.wechsleWunschKarte(Farbe.Blau, Wert.Draw_Four);
                        } else if (Wert.Multicolor == aktuelleKarte.getWert()) {
                            spielfeld.wechsleWunschKarte(Farbe.Blau, Wert.Multicolor);
                        }
                    }
                    case 3 -> {
                        regel.color.add(Farbe.Rot);
                        if (Wert.Draw_Four == aktuelleKarte.getWert()) {
                            spielfeld.wechsleWunschKarte(Farbe.Rot, Wert.Draw_Four);
                        } else if (Wert.Multicolor == aktuelleKarte.getWert()) {
                            spielfeld.wechsleWunschKarte(Farbe.Rot, Wert.Multicolor);
                        }
                    }
                    default -> {
                    }
                }
            }
        }
        return kartenIndex;
    }

    public void legbareKarten(SpielerHand spielerHand,int aktuellerSpieler,UnoKarte aktuelleKarte, Regelwerk regel){
        int size = spielerHand.spielerHaende.get(aktuellerSpieler).size();
        boolean geht = true;
        for(int i = 0; i < size; i++){
            UnoKarte neueKarte = spielerHand.spielerHaende.get(aktuellerSpieler).get(i);
            if(geht == ueberpruefeKarte(aktuelleKarte, neueKarte, regel)){
                LegbareKarten.add(neueKarte);
            }
        }
    }

    public Boolean ueberpruefeKarte(UnoKarte aktuelleKarte, UnoKarte neueKarte, Regelwerk regel){
        if(regel.color.size() == 2) {
            if(regel.color.get(1) == neueKarte.getFarbe() || regel.color.get(0) == neueKarte.getFarbe()){
                return true;
            }else {
                return false;
            }
        }else if (aktuelleKarte.getFarbe() == neueKarte.getFarbe()) {
            return true;
        } else if (aktuelleKarte.getWert() == neueKarte.getWert()) {
            return true;
        }else if(neueKarte.getFarbe() == Farbe.Wild || aktuelleKarte.getFarbe() == Farbe.Wild){
            return true;
        } else {
            return false;
        }
    }

    public static void sortiereUnoKarten(ArrayList<UnoKarte> LegbareKarten){
        Collections.sort(LegbareKarten, new Comparator<UnoKarte>() {
            @Override
            public int compare(UnoKarte karte1, UnoKarte karte2) {
                int farbenIndex1 = getFarbenIndex(karte1.getFarbe());
                int farbenIndex2 = getFarbenIndex(karte2.getFarbe());

                return Integer.compare(farbenIndex1, farbenIndex2);
            }
        });
    }

    public static int getFarbenIndex(Farbe farbe) {
        switch (farbe){
            case Rot -> {
                return 0;
            }case Blau -> {
                return 1;
            }case Gelb -> {
                return 2;
            }case Gruen -> {
                return 3;
            }case Wild -> {
                return 4;
            }default -> {
                return -1;
            }
        }
    }
}

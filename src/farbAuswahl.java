/*
    An dieser Datei hat Ismail Eren Cakan größtenteils gearbeitet
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class farbAuswahl extends JFrame {
    private JButton kartenAuswahlButton1 = createImageButton("UnoKarten/BackSide.png");
    private JButton kartenAuswahlButton2 = createImageButton("UnoKarten/BackSide.png");
    private JButton kartenAuswahlButton3 = createImageButton("UnoKarten/BackSide.png");
    private JButton kartenAuswahlButton4 = createImageButton("UnoKarten/BackSide.png");
    private JButton kartenAuswahlButton5 = createImageButton("UnoKarten/BackSide.png");
    private JButton kartenAuswahlButton6 = createImageButton("UnoKarten/BackSide.png");
    private JButton kartenAuswahlButton7 = createImageButton("UnoKarten/BackSide.png");
    private JButton kartenAuswahlButton8 = createImageButton("UnoKarten/BackSide.png");
    public void farbAuswahl(UnoKarte neueKarte,Regelwerk regel,UnoSpielfeld spielFeld){
        if(neueKarte.getFarbe() == Farbe.Wild){
            setTitle("Farbe Wählen");
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            setLayout(new BorderLayout());

            JPanel spielfeldPanel2 = new JPanel(new BorderLayout());
            add(spielfeldPanel2, BorderLayout.CENTER);

            JPanel centerPanel = new JPanel();
            spielfeldPanel2.add(centerPanel, BorderLayout.CENTER);
            centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.X_AXIS));

            centerPanel.add(kartenAuswahlButton1);
            centerPanel.add(kartenAuswahlButton2);
            centerPanel.add(kartenAuswahlButton3);
            centerPanel.add(kartenAuswahlButton4);
            centerPanel.add(kartenAuswahlButton5);
            centerPanel.add(kartenAuswahlButton6);
            centerPanel.add(kartenAuswahlButton7);
            centerPanel.add(kartenAuswahlButton8);

            ImageIcon newIcon;

            if(neueKarte.getWert() == Wert.Multicolor) {
                centerPanel.remove(kartenAuswahlButton5);
                centerPanel.remove(kartenAuswahlButton6);
                centerPanel.remove(kartenAuswahlButton7);
                centerPanel.remove(kartenAuswahlButton8);
                for(int g = 0; g < 4; g++) {
                    switch (g){
                        case 0:
                            kartenAuswahlButton1.setIcon(null);
                            newIcon = new ImageIcon("UnoKarten/Wild_Multicolor_Rot.png");    // Passe den Pfad zum Uno-Bild an
                            kartenAuswahlButton1.setIcon(newIcon);
                            revalidate();
                            repaint();
                            kartenAuswahlButton1.addActionListener(new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent e) {
                                    regel.color.add(Farbe.Rot);
                                    spielFeld.wechsleWunschKarte(Farbe.Rot,Wert.Multicolor);
                                    dispose();
                                }
                            });
                            break;
                        case 1:
                            kartenAuswahlButton2.setIcon(null);
                            newIcon = new ImageIcon("UnoKarten/Wild_Multicolor_Blau.png");    // Passe den Pfad zum Uno-Bild an
                            kartenAuswahlButton2.setIcon(newIcon);
                            revalidate();
                            repaint();
                            kartenAuswahlButton2.addActionListener(new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent e) {
                                    regel.color.add(Farbe.Blau);
                                    spielFeld.wechsleWunschKarte(Farbe.Blau,Wert.Multicolor);
                                    dispose();
                                }
                            });
                            break;
                        case 2:
                            kartenAuswahlButton3.setIcon(null);
                            newIcon = new ImageIcon("UnoKarten/Wild_Multicolor_Gruen.png");    // Passe den Pfad zum Uno-Bild an
                            kartenAuswahlButton3.setIcon(newIcon);
                            revalidate();
                            repaint();
                            kartenAuswahlButton3.addActionListener(new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent e) {
                                    regel.color.add(Farbe.Gruen);
                                    spielFeld.wechsleWunschKarte(Farbe.Gruen,Wert.Multicolor);
                                    dispose();
                                }
                            });
                            break;
                        case 3:
                            kartenAuswahlButton4.setIcon(null);
                            newIcon = new ImageIcon("UnoKarten/Wild_Multicolor_Gelb.png");    // Passe den Pfad zum Uno-Bild an
                            kartenAuswahlButton4.setIcon(newIcon);
                            revalidate();
                            repaint();
                            kartenAuswahlButton4.addActionListener(new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent e) {
                                    regel.color.add(Farbe.Gelb);
                                    spielFeld.wechsleWunschKarte(Farbe.Gelb,Wert.Multicolor);
                                    dispose();
                                }
                            });
                            break;
                        default:
                            throw new IllegalStateException("Unexpected value: " + g);
                    }
                }
            }
            else if(neueKarte.getWert() == Wert.Draw_Four) {
                centerPanel.remove(kartenAuswahlButton1);
                centerPanel.remove(kartenAuswahlButton2);
                centerPanel.remove(kartenAuswahlButton3);
                centerPanel.remove(kartenAuswahlButton4);

                for(int g = 0; g < 4; g++) {
                    switch (g){
                        case 0:
                            kartenAuswahlButton5.setIcon(null);
                            newIcon = new ImageIcon("UnoKarten/Wild_Draw_Four_Rot.png");    // Passe den Pfad zum Uno-Bild an
                            kartenAuswahlButton5.setIcon(newIcon);
                            revalidate();
                            repaint();
                            kartenAuswahlButton5.addActionListener(new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent e) {
                                    regel.color.add(Farbe.Rot);
                                    spielFeld.wechsleWunschKarte(Farbe.Rot,Wert.Draw_Four);
                                    dispose();
                                }
                            });
                            break;
                        case 1:
                            kartenAuswahlButton6.setIcon(null);
                            newIcon = new ImageIcon("UnoKarten/Wild_Draw_Four_Blau.png");    // Passe den Pfad zum Uno-Bild an
                            kartenAuswahlButton6.setIcon(newIcon);
                            revalidate();
                            repaint();
                            kartenAuswahlButton6.addActionListener(new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent e) {
                                    regel.color.add(Farbe.Blau);
                                    spielFeld.wechsleWunschKarte(Farbe.Blau,Wert.Draw_Four);
                                    dispose();
                                }
                            });
                            break;
                        case 2:
                            kartenAuswahlButton7.setIcon(null);
                            newIcon = new ImageIcon("UnoKarten/Wild_Draw_Four_Gruen.png");    // Passe den Pfad zum Uno-Bild an
                            kartenAuswahlButton7.setIcon(newIcon);
                            revalidate();
                            repaint();
                            kartenAuswahlButton7.addActionListener(new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent e) {
                                    regel.color.add(Farbe.Gruen);
                                    spielFeld.wechsleWunschKarte(Farbe.Gruen,Wert.Draw_Four);
                                    dispose();
                                }
                            });
                            break;
                        case 3:
                            kartenAuswahlButton8.setIcon(null);
                            newIcon = new ImageIcon("UnoKarten/Wild_Draw_Four_Gelb.png");    // Passe den Pfad zum Uno-Bild an
                            kartenAuswahlButton8.setIcon(newIcon);
                            revalidate();
                            repaint();
                            kartenAuswahlButton8.addActionListener(new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent e) {
                                    regel.color.add(Farbe.Gelb);
                                    spielFeld.wechsleWunschKarte(Farbe.Gelb,Wert.Draw_Four);
                                    dispose();
                                }
                            });
                            break;
                        default:
                            throw new IllegalStateException("Unexpected value: " + g);
                    }
                }
            }
            revalidate();
            repaint();
            pack();
            setVisible(true);
        }
        //Es wird ein Fenster geöffnet, indem man seine wunschfarbe Auswählen kann
        //Manchmal werden nicht alle Bilder geladen, dann muss man mit der Maus drüber fahren!!!!!!
    }
    public JButton createImageButton(String imagePath) {
        ImageIcon icon = new ImageIcon(imagePath);
        JButton button = new JButton(icon);
        button.setBorderPainted(false);
        button.setFocusPainted(false);
        button.setContentAreaFilled(false);
        return button;
    }
}


/*
    Der Bot bewertet Karten nach einem Punkte System. Je höher die Punkte um so nerviger ist die Karte
 */

// An diesem Bot haben Ismail Eren Cakan und Furkan Celik gearbeitet
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Bot_2 {
    private boolean neuKarte = false;

    private ArrayList<UnoKarte> LegbareKarten;
    public int Bot_2(SpielerHand spielerHand,int aktuellerSpieler, UnoKarte aktuelleKarte, Regelwerk regel,UnoDeck deck, UnoSpielfeld spielfeld,BotInterface anzeige,boolean Interface){
        UnoKarte alteKarte = aktuelleKarte;
        LegbareKarten = new ArrayList<UnoKarte>();
        legbareKarten(spielerHand,aktuellerSpieler,aktuelleKarte,regel);
        sortiereUnoKarten(LegbareKarten);
        int lenght = spielerHand.spielerHaende.get(aktuellerSpieler).size();
        int size = LegbareKarten.size();

        if(Interface == true) {
            String Bot = "Bot 2";
            anzeige.anzeige(LegbareKarten, spielerHand, aktuellerSpieler, Bot);
        }
        if(size >=1) {
            UnoKarte besteKarte = LegbareKarten.get(0);
            int besteBewertung = bewerteKarte(besteKarte, aktuelleKarte);

            for (UnoKarte karte : LegbareKarten) {
                int bewertung = bewerteKarte(karte, aktuelleKarte);
                if (bewertung > besteBewertung) {
                    besteBewertung = bewertung;
                    besteKarte = karte;
                }
            }
            aktuelleKarte = besteKarte;
            neuKarte = true;
        } else if (size == 0) {
            spielerHand.befuelleSpieleHand(aktuellerSpieler,deck,1);
            lenght = spielerHand.spielerHaende.get(aktuellerSpieler).size();
            UnoKarte neueKarte = spielerHand.spielerHaende.get(aktuellerSpieler).get(lenght-1);
            if(true == ueberpruefeKarte(aktuelleKarte,neueKarte,regel)){
                aktuelleKarte = neueKarte;
                neuKarte = true;
            }
        }else {
            spielerHand.befuelleSpieleHand(aktuellerSpieler,deck,1);
            lenght = spielerHand.spielerHaende.get(aktuellerSpieler).size();
            UnoKarte neueKarte = spielerHand.spielerHaende.get(aktuellerSpieler).get(lenght-1);
            if(true == ueberpruefeKarte(aktuelleKarte,neueKarte,regel)){
                aktuelleKarte = neueKarte;
                neuKarte = true;
            }
        }

        int kartenIndex = -1;
        if(neuKarte == true) {
            if (regel.color.size() == 2) {
                if (aktuelleKarte.getFarbe() == regel.color.get(1) || aktuelleKarte.getFarbe() == regel.color.get(0)) {
                    if (aktuelleKarte.getFarbe() == regel.color.get(0)) {
                        regel.color.remove(1);
                    }
                    for (int i = 0; i < lenght; i++) {
                        if (aktuelleKarte.equals(spielerHand.spielerHaende.get(aktuellerSpieler).get(i))) {
                            kartenIndex = i;
                        }
                    }
                }
            } else {
                for (int i = 0; i < lenght; i++) {
                    if (aktuelleKarte.equals(spielerHand.spielerHaende.get(aktuellerSpieler).get(i))) {
                        kartenIndex = i;
                    }
                }
            }
        }

        int[] farben = {0,0,0,0,0};
        if((aktuelleKarte.getWert() == Wert.Draw_Four || aktuelleKarte.getWert() == Wert.Multicolor) && neuKarte == true){
            for(UnoKarte karte : spielerHand.spielerHaende.get(aktuellerSpieler)){
                switch (karte.getFarbe()){
                    case Gruen ->
                            farben[0] +=1;
                    case Gelb ->
                            farben[1] +=1;
                    case Blau ->
                            farben[2] +=1;
                    case Rot ->
                            farben[3] +=1;
                    case Wild ->
                            farben[4] +=1;
                }
            }
            int max = farben[0];
            int id = 0;

            for (int i = 1; i < farben.length; i++) {
                if (farben[i] > max) {
                    max = farben[i];
                    id = i;
                }
            }
            switch (id){
                case 0 -> {
                    if(alteKarte.getFarbe() == Farbe.Gruen){

                    }else {
                        regel.color.add(Farbe.Gruen);
                    }
                    if (Wert.Draw_Four == aktuelleKarte.getWert()) {
                        spielfeld.wechsleWunschKarte(Farbe.Gruen, Wert.Draw_Four);
                    } else if (Wert.Multicolor == aktuelleKarte.getWert()) {
                        spielfeld.wechsleWunschKarte(Farbe.Gruen, Wert.Multicolor);
                    }
                }
                case 1 -> {
                    if(alteKarte.getFarbe() == Farbe.Gelb){

                    }else {
                        regel.color.add(Farbe.Gelb);
                    }
                    if (Wert.Draw_Four == aktuelleKarte.getWert()) {
                        spielfeld.wechsleWunschKarte(Farbe.Gelb, Wert.Draw_Four);
                    } else if (Wert.Multicolor == aktuelleKarte.getWert()) {
                        spielfeld.wechsleWunschKarte(Farbe.Gelb, Wert.Multicolor);
                    }
                }
                case 2 -> {
                    if(alteKarte.getFarbe() == Farbe.Blau){

                    }else {
                        regel.color.add(Farbe.Blau);
                    }
                    if (Wert.Draw_Four == aktuelleKarte.getWert()) {
                        spielfeld.wechsleWunschKarte(Farbe.Blau, Wert.Draw_Four);
                    } else if (Wert.Multicolor == aktuelleKarte.getWert()) {
                        spielfeld.wechsleWunschKarte(Farbe.Blau, Wert.Multicolor);
                    }
                }
                case 3 -> {
                    if(alteKarte.getFarbe() == Farbe.Rot){

                    }else {
                        regel.color.add(Farbe.Rot);
                    }
                    if (Wert.Draw_Four == aktuelleKarte.getWert()) {
                        spielfeld.wechsleWunschKarte(Farbe.Rot, Wert.Draw_Four);
                    } else if (Wert.Multicolor == aktuelleKarte.getWert()) {
                        spielfeld.wechsleWunschKarte(Farbe.Rot, Wert.Multicolor);
                    }
                }
                default -> {
                }
            }
        }

        return kartenIndex;
    }

    public void legbareKarten(SpielerHand spielerHand,int aktuellerSpieler,UnoKarte aktuelleKarte, Regelwerk regel){
        int size = spielerHand.spielerHaende.get(aktuellerSpieler).size();
        boolean geht = true;
        for(int i = 0; i < size; i++){
            UnoKarte neueKarte = spielerHand.spielerHaende.get(aktuellerSpieler).get(i);
            if(geht == ueberpruefeKarte(aktuelleKarte, neueKarte, regel)){
                LegbareKarten.add(neueKarte);
            }
        }
    }

    public Boolean ueberpruefeKarte(UnoKarte aktuelleKarte, UnoKarte neueKarte, Regelwerk regel){
        if(regel.color.size() == 2) {
            if(regel.color.get(1) == neueKarte.getFarbe() || regel.color.get(0) == neueKarte.getFarbe()){
                return true;
            }else {
                return false;
            }
        }else if (aktuelleKarte.getFarbe() == neueKarte.getFarbe()) {
            return true;
        } else if (aktuelleKarte.getWert() == neueKarte.getWert()) {
            return true;
        }else if(neueKarte.getFarbe() == Farbe.Wild || aktuelleKarte.getFarbe() == Farbe.Wild){
            return true;
        } else {
            return false;
        }
    }

    public static void sortiereUnoKarten(ArrayList<UnoKarte> LegbareKarten){
        Collections.sort(LegbareKarten, new Comparator<UnoKarte>() {
            @Override
            public int compare(UnoKarte karte1, UnoKarte karte2) {
                int farbenIndex1 = getFarbenIndex(karte1.getFarbe());
                int farbenIndex2 = getFarbenIndex(karte2.getFarbe());

                return Integer.compare(farbenIndex1, farbenIndex2);
            }
        });
    }

    public static int getFarbenIndex(Farbe farbe) {
        switch (farbe){
            case Rot -> {
                return 0;
            }case Blau -> {
                return 1;
            }case Gelb -> {
                return 2;
            }case Gruen -> {
                return 3;
            }case Wild -> {
                return 4;
            }default -> {
                return -1;
            }
        }
    }

    public static int bewerteKarte(UnoKarte besteKarte, UnoKarte aktuelleKarte){
        int bewertung = 0;

        if(besteKarte.getWert() == Wert.Zwei_Ziehen){
            bewertung +=20;
        }else if(besteKarte.getWert() == Wert.Draw_Four){
            bewertung += 50;
        }else if(besteKarte.getWert() == Wert.Aussetzen || besteKarte.getWert() == Wert.Richtungswechsel){
            bewertung += 10;
        }

        if(besteKarte.getFarbe() == Farbe.Wild){
            bewertung += 3;
        }

        if(besteKarte.getFarbe() != aktuelleKarte.getFarbe()) {
            bewertung += 5;
        }else if(besteKarte.getWert() != aktuelleKarte.getWert()){
            bewertung += 2;
        }
        return bewertung;
    }
}

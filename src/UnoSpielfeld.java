/*
    An dieser Datei hat Justin Klein gearbeitet
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class UnoSpielfeld extends JFrame {
    private JPanel spielfeldPanel;
    private JPanel spielerPanel;
    private JPanel buttonsPanel;
    private JScrollPane kartenScrollPane;
    private JPanel kartenPanel;
    private JPanel centerPanel;
    public JButton zugButton;
    public JButton unoButton;
    public JButton karteLegen;
    public JButton karteBehalten;
    public JLabel kartenLabel;
    public JButton zieheButton;
    public JButton farbeundo = null;
    public boolean zugStarten = false;
    public boolean kartelegen = false;
    public boolean karteziehen = false;
    public boolean neuekartelegen = false;
    public boolean neuekartebehalten = false;
    public boolean uno = false;
    public int Index;
    public int spielerIndex = 0;
    private int size = 0;
    public Wert wert;
    public Farbe farbe;
    Object lock = null;
    private ArrayList<JButton> spielerButton;
    private ArrayList<Boolean> isBot;


    public UnoSpielfeld(UnoKarte aktuelleKarte, SpielerHand spilerhand, Regelwerk regel,Object lock1,int lenght,ArrayList<Boolean> ifBot) {
        this.lock = lock1;

        spielerButton = new ArrayList<JButton>();
        isBot = new ArrayList<Boolean>();
        isBot = ifBot;

        size = lenght;
        // Fenstereinstellungen
        setTitle("Uno Spielfeld");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());


        // Spielfeldpanel
        spielfeldPanel = new JPanel(new BorderLayout());
        add(spielfeldPanel, BorderLayout.CENTER);

        // Kartenanzeige
        kartenPanel = new JPanel();
        kartenPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        kartenScrollPane = new JScrollPane(kartenPanel);
        spielfeldPanel.add(kartenScrollPane, BorderLayout.SOUTH);

        // Spielerliste
        spielerPanel = new JPanel();
        spielfeldPanel.add(spielerPanel, BorderLayout.EAST);

        // Knöpfe
        buttonsPanel = new JPanel();
        spielfeldPanel.add(buttonsPanel, BorderLayout.WEST);

        //Spielfeld
        centerPanel = new JPanel();
        spielfeldPanel.add(centerPanel, BorderLayout.CENTER);

        // Knöpfe hinzufügen
        zugStartenKnopf(spilerhand,regel);
        unoKnopf();
        buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.Y_AXIS));

        // Spieler hinzufügen
        SpielerHinzufuegen(spilerhand);

        karteBehalten();

        //Karten ziehe Platz hinzufügen
        zieheKarte();

        ImageIcon kartenIcon = new ImageIcon("UnoKarten/BackSide.png"); // Passe den Pfad zum Kartenbild an
        kartenLabel = new JLabel(kartenIcon);
        centerPanel.add(kartenLabel);

        karteLegen();


        setSize(1200,700);
        setVisible(true);
        //Es wird das Spielfeld erstellt mit der größe 1200 x 700
    }

    public JButton createImageButton(String imagePath) {
        ImageIcon icon = new ImageIcon(imagePath);
        JButton button = new JButton(icon);
        button.setBorderPainted(false);
        button.setFocusPainted(false);
        button.setContentAreaFilled(false);
        return button;
    }
    public void SpielerHinzufuegen(SpielerHand spielerHand){
        for(int j = 0; j < size ; j++){
            int w = j;
            int finalJ = j;
            JButton spielerLabel = null;
            if(isBot.get(w) == false){
                 spielerLabel = new JButton("Spieler " + (w + 1) + ": \t " + spielerHand.spielerNamen[j]);
            }else if (isBot.get(w) == true){
                 spielerLabel = new JButton("Bot " + (w + 1) + ": \t " + spielerHand.spielerNamen[j]);
            }
            spielerPanel.add(spielerLabel);
            spielerButton.add(spielerLabel);
            spielerLabel.setFont(new Font("Arial", Font.PLAIN, 20));
            spielerLabel.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    JOptionPane.showMessageDialog(null,"Spieler " + spielerHand.spielerNamen[finalJ] + " besitzt " + spielerHand.spielerHaende.get(finalJ).size() + " Karten") ;
                }
            });
        }
        spielerPanel.setLayout(new BoxLayout(spielerPanel, BoxLayout.Y_AXIS));

        //Es werden die Spieler angezeigt
        //Beim drücken des Spielers öffnet sich ein Fenster mit der Anzahl der karten des Spielers
    }

    public void colorPlayer(int reihen){
        if (farbeundo != null) {
            farbeundo.setBackground(null);
        }
        JButton farbe = spielerButton.get(reihen);
        farbeundo = farbe;
        farbe.setBackground(Color.decode("#F2F723"));

        //Es werde der Spieler der gerade am zug ist sichtbar angezeigt
    }
    public void karteBehalten(){
        karteBehalten = new JButton("Behalten");
        centerPanel.add(karteBehalten);
        karteBehalten.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                neuekartebehalten = true;
                try {
                    Thread.sleep(150);
                } catch (InterruptedException ex) {
                    throw new RuntimeException(ex);
                }
                neuekartebehalten = false;
            }
        });
        //Der knopf zur Behalten der Karte und seine funktion werden Hier erstellt
    }
    public void karteLegen(){
        karteLegen = new JButton("Legen");
        centerPanel.add(karteLegen);
        karteLegen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                neuekartelegen = true;
                try {
                    Thread.sleep(150);
                } catch (InterruptedException ex) {
                    throw new RuntimeException(ex);
                }
                neuekartelegen = false;
            }
        });
        //Der knopf zum Legen der Karte und seine funktion werden Hier erstellt
    }

    public void zieheKarte(){
        zieheButton = createImageButton("UnoKarten/BackSide.png");
        centerPanel.add(zieheButton);
        zieheButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                karteziehen = true;
                try {
                    Thread.sleep(150);
                } catch (InterruptedException ex) {
                    throw new RuntimeException(ex);
                }
                    synchronized (lock) {
                        lock.notify();
                    }
                karteziehen = false;
            }
        });
        //Der Stapel zum ziehen der Karte wird erstellt und bekommt das Bild von der Rückseite einer Uno Karte
    }

    public void changeKarteziehen(UnoKarte gezogeneKarte){
        ImageIcon newIcon = new ImageIcon("UnoKarten/"+Bild(gezogeneKarte)+".png");    // Passe den Pfad zum Uno-Bild an
        zieheButton.setIcon(newIcon);

        //Diese funktion ändert das Bild des Stapels zu dem Bild welches der Spiler neu gezogen hat
    }

    public void changeKarteziehenundo(){
        ImageIcon newIcon = new ImageIcon("UnoKarten/BackSide.png");    // Passe den Pfad zum Uno-Bild an
        zieheButton.setIcon(newIcon);
        //Diese Methode ändert das Bild wieder zur Rückseite einer Karte
    }

    public void aktuelleKarte(UnoKarte aktuelleKarte){
        ImageIcon oldIcon = (ImageIcon)  kartenLabel.getIcon();
        kartenLabel.setIcon(null);

        revalidate();
        repaint();

        ImageIcon newIcon = new ImageIcon("UnoKarten/" + Bild(aktuelleKarte) + ".png");    // Passe den Pfad zum Uno-Bild an
        kartenLabel.setIcon(newIcon);
        if(aktuelleKarte.getFarbe() == Farbe.Wild) {
            if (wert == Wert.Multicolor) {
                newIcon = new ImageIcon("UnoKarten/Wild_" + wert + "_" + farbe + ".png");    // Passe den Pfad zum Uno-Bild an
                kartenLabel.setIcon(newIcon);
            } else if (wert == Wert.Draw_Four) {
                newIcon = new ImageIcon("UnoKarten/Wild_" + wert + "_" + farbe + ".png");    // Passe den Pfad zum Uno-Bild an
                kartenLabel.setIcon(newIcon);
            }
            revalidate();
            repaint();
        }
        //Es wird die Aktuelle Karte erstellt, welche zuletzt gelegt wurde
    }

    public void KarteHinzufuegen( SpielerHand spielerHand, int reihen){
        spielerIndex = reihen;
        int size = spielerHand.spielerHaende.get(reihen).size()-1;
        kartenPanel.removeAll();
        revalidate();
        repaint();
        for (int i = 0; i <= size; i++) {
            JButton kartenButton = createImageButton("UnoKarten/"+Bild(spielerHand.spielerHaende.get(reihen).get(i))+".png"); // Passe den Pfad zum Kartenbild an
            int finalI = i;
            kartenButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    kartelegen = true;
                    Index = finalI;
                    try {
                        Thread.sleep(150);
                    } catch (InterruptedException ex) {
                        throw new RuntimeException(ex);
                    }
                    kartelegen = false;
                }
            });
            kartenPanel.add(kartenButton);
        }
        //es werden die Aktuellen Karten des Spilers der gerade am zug ist angezeigt
    }
    public void unoKnopf(){
        unoButton = new JButton("Uno");
        buttonsPanel.add(unoButton);
        unoButton.setFont(new Font("Arial", Font.PLAIN, 20));
        unoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                uno = true;
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException ex) {
                    throw new RuntimeException(ex);
                }
                uno = false;
            }
        });
        //Es wird ein Uno Knopf erstellt, mit seiner funktion
    }

    public void zugStartenKnopf(SpielerHand spielerHand, Regelwerk regel){
        zugButton = new JButton("Zug Starten");
        buttonsPanel.add(zugButton);
        zugButton.setFont(new Font("Arial", Font.PLAIN, 20));
        zugButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                zugStarten = true;
                try {
                    Thread.sleep(150);
                } catch (InterruptedException ex) {
                    throw new RuntimeException(ex);
                }
                zugStarten = false;
            }
        });
        //Es wird ein Knopf erstellt der den nächsten zug Starten lässt
    }
    public void Kartenausschalten(){
        kartenPanel.removeAll();
        revalidate();
        repaint();
    }

    public void wechsleWunschKarte(Farbe wunschFarbe, Wert wunschWert){
        farbe = wunschFarbe;
        wert = wunschWert;
    }

    public void beenden(){
        dispose();
    }

    public String Bild(UnoKarte aktuelleKarte){
        Farbe farbe = aktuelleKarte.getFarbe();
        Wert wert = aktuelleKarte.getWert();
        String wort = farbe + "_" + wert;
        return wort;

    }
    public void highUno(){
        unoButton.setBackground(Color.decode("#F2F723"));
    }
    public void highUnoundo(){
        unoButton.setBackground(null);
    }
    public void highBehalten(){
        karteBehalten.setBackground(Color.decode("#F2F723"));
    }
    public void highBehaltenundo(){
        karteBehalten.setBackground(null);
    }
    public void highLegen(){
        karteLegen.setBackground(Color.decode("#F2F723"));
    }
    public void highLegenundo(){
        karteLegen.setBackground(null);
    }
    public void highZugStarten(){
        zugButton.setBackground(Color.decode("#F2F723"));
    }
    public void highZugStartenundo(){
        zugButton.setBackground(null);
    }

    public boolean getzugStarten(){
        return zugStarten;
    }
    public boolean getkartelegen(){
        return kartelegen;
    }
    public boolean getkarteziehen(){
        return karteziehen;
    }
    public boolean getneuekartelegen(){ return neuekartelegen; }
    public boolean getneuekartebehalten(){ return neuekartebehalten; }
    public boolean getUno(){ return uno; }
    public int getKartenIndex(){ return Index; }
    public int getSpielerIndex(){ return spielerIndex; }
}

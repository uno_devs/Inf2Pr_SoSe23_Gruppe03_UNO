//Diese Klasse soll denn Bots ihre jeweilige Boteigenschaft geben und sie speichern

//Daran hat Justin Klein gearbeitet
import java.util.ArrayList;
import java.util.Random;
public class BotKontroll {

    private ArrayList<Integer> isBot;
    public int BotKontroll(SpielerHand spielerHand,int aktuellerSpieler, UnoKarte aktuelleKarte, Regelwerk regel,UnoDeck deck, UnoSpielfeld spielfeld,BotInterface anzeige, boolean Interface) {
        Random rand = new Random();

        if (isBot.get(aktuellerSpieler) == null) {
            int randome = rand.nextInt(1,4);
            isBot.set(aktuellerSpieler, randome);
        }

        int kartenIndex = -1;
        switch (isBot.get(aktuellerSpieler)){
            case 1 -> {
                Bot_1 bot_1 = new Bot_1();
                kartenIndex = bot_1.Bot_1(spielerHand, aktuellerSpieler, aktuelleKarte, regel, deck, spielfeld, anzeige, Interface);
            }case 2 -> {
                Bot_2 bot_2 = new Bot_2();
                kartenIndex = bot_2.Bot_2(spielerHand, aktuellerSpieler, aktuelleKarte, regel, deck, spielfeld, anzeige, Interface);
            }case 3 ->{
                Bot_3 bot_3 = new Bot_3();
                kartenIndex = bot_3.Bot_3(spielerHand, aktuellerSpieler, aktuelleKarte, regel, deck, spielfeld, anzeige, Interface);
            }
        }
        return kartenIndex;
    }

    public void setBotKontroll(int lenght){
        isBot = new ArrayList<>();

        for(int i = 0; i < lenght; i++){
            isBot.add(null);
        }
    }
}
